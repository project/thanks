<?php
/**
* @file
* The administration form for the thanks module.
*/

function thanks_settings_form() {
  $settings = variable_get('thanks', thanks_defaults());

  $form['thanks'] = array(
    '#tree' => TRUE,
  );

  $form['thanks']['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable thanks for the following node types'),
    '#tree' => TRUE,
  );
  
  foreach (node_get_types('names') as $type => $name) {
    $form['thanks']['node_types']['thanks_show_for_' . $type] = array(
      '#type' => 'checkbox',
      '#title' => $name,
      '#default_value' => (isset($settings['node_types']['thanks_show_for_' . $type]) ? $settings['node_types']['thanks_show_for_' . $type] : FALSE),   
    );
  }

  $form['thanks']['text'] = array(
    '#type' => 'textfield',
    '#title' => 'Display text',
    '#description' => 'The text that will appear in the thanks box',
    '#default_value' => $settings['text'],
  );

  $form['thanks']['thank_own_posts'] = array(
    '#type' => 'checkbox',
    '#title' => 'Allow users to thank their own posts',
    '#default_value' => $settings['thank_own_posts'],
  );

  $form['thanks']['thank_teasers'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable thanks on teasers',
    '#default_value' => $settings['thank_teasers'],
  );

  $form['thanks']['show_on_profiles'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show thanks received and thanks given on user profiles',
    '#default_value' => $settings['show_on_profiles'],
  );
  return system_settings_form($form);
}

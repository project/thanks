
Instructions:
1. Enable thanks module

2. Set user permissions for thanks module

3. Enable thanks on particular node types and adjust other settings at admin/settings/thanks

4a. Add the following code just before the final closing </div> in your node.tpl.php and comment.tpl.php files -
  <?php if ($thanks): ?>
    <?php print $thanks; ?>
  <?php endif;?>
  
4b. For advanced forum pages add the following code to the advanced_forum.naked.post.tpl.php
just before the text: </div> <?php // End of main wrapping div ?>

  <?php if ($thanks): ?>
    <?php print $thanks; ?>
  <?php endif; ?>


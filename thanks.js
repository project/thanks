
Drupal.Thanks = Drupal.Thanks || {};

Drupal.behaviors.Thanks = function(context){
  // Add a click handler to the thanks buttons
  $('a.thanks:not(.thanks-processed)').addClass('thanks-processed').click(function(event){

    // Get the details of this thanks link
    var info = $(this).attr('id').split('-');
    var action = info[1];
    var nid = info[2];
    var cid = info[3];
    var target = this;
  
    // Build the ajax path
    ajax_path = Drupal.settings.basePath + 'handle_thanks/' + action + '/' + nid + '/' + cid;
    
    $.ajax({
      url: ajax_path,
      type: 'GET',
      data: null,
      dataType: 'json',
      async: false,
      success: function(response){
        // Toggle the thanks link and add or remove the user name from the thanks box
        Drupal.Thanks.toggleLink(target, action, nid, cid);
    	
        // Check if the link belongs to a forum post, a comment or a node
        var parent = null;
        if ($(target).parents('.forum-post').size() > 0){
          parent = $(target).parents('.forum-post');
        }
        else if ($(target).parents('.comment').size() > 0){
          parent = $(target).parents('.comment');
        }
        else {
          parent = $(target).parents('.node');
        }
    		  
        // Replace the thanks box with the updated one  	  
        if ($(parent).find('.thanks-box').size() > 0){
          $(parent).find('.thanks-box').replaceWith(response.thanks_box);
        }
        // No existing thanks box so append it to the parent
        else{
          $(parent).append(response.thanks_box);
        }
      }
    });
    return false;
  });
};

/**
 * Toggle the thanks link between thank and remove thanks
 */
Drupal.Thanks.toggleLink = function(target, action, nid, cid){
  id = $(target).attr('id');
  if (action == 'add')
    $(target).attr('id', 'thanks-remove-' + nid + '-' + cid).attr('title', 'Remove thanks from this post').text('remove thanks');
  else
    $(target).attr('id', 'thanks-add-' + nid + '-' + cid).attr('title', 'Thank this post').text('thank');
};
<?php 

/**
 * Implementation of hook_views_data()
 */

function thanks_views_data() {
  // Basic table information.
  $data['thanks_stats']['table']['group']  = t('Thanks');
  
    // Join to 'users' as a base table.
  $data['thanks_stats']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  
  $data['thanks_stats']['thanks_given'] = array(
    'title' => t('Thanks given'),
    'help' => t('The number of time this user has given thanks.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  $data['thanks_stats']['thanks_recieved'] = array(
    'title' => t('Thanks recieved'),
    'help' => t('The number of time this user has received thanks.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  ); 
  
  return $data; 
}
